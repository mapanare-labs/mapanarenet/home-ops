---
apiVersion: helm.toolkit.fluxcd.io/v2
kind: HelmRelease
metadata:
  name: authentik
spec:
  interval: 15m
  chart:
    spec:
      chart: authentik
      version: 2025.2.1
      sourceRef:
        kind: HelmRepository
        name: authentik
        namespace: flux-system
  install:
    createNamespace: true
    remediation:
      retries: 3
  upgrade:
    cleanupOnFail: true
    remediation:
      retries: 3
  uninstall:
    keepHistory: false
  values:
    global:
      deploymentAnnotations:
        secret.reloader.stakater.com/reload: &secret authentik-secret
      affinity:
        nodeAffinity:
          type: hard
          matchExpressions:
            - key: kubernetes.io/arch
              operator: In
              values:
                - amd64
                - i386
                - i686
                - x86
    server:
      replicas: 1
      initContainers:
        - name: init-db
          image: ghcr.io/onedr0p/postgres-init:16
          imagePullPolicy: IfNotPresent
          envFrom:
            - secretRef:
                name: *secret
      metrics:
        enabled: true
        serviceMonitor:
          enabled: true
      resources:
        requests:
          cpu: 50m
          memory: 500Mi
        limits:
          memory: 800Mi
      ingress:
        enabled: true
        ingressClassName: internal
        hosts:
          - &host "sso.mapanare.net"
        paths:
          - /
        pathType: Prefix
        tls:
          - secretName: *host
            hosts:
              - *host
    worker:
      replicas: 1
      resources:
        requests:
          cpu: 50m
          memory: 500Mi
        limits:
          memory: 1200Mi
    authentik:
      postgresql:
        port: "5000"
      redis:
        host: "dragonfly.databases.svc.cluster.local"
        password: ""
      email:
        use_ssl: true
        port: "465"
        from: "Authentik <sso@mapanare.net>"
      log_level: debug
      error_reporting:
        enable: false
        send_pii: false
      outposts:
        docker_image_base: ghcr.io/goauthentik/%(type)s:%(version)s
    geoip:
      enabled: false
      existingSecret:
        secretName: *secret
        accountId: ACCOUNT_ID
        licenseKey: LICENSE_KEY
    prometheus:
      rules:
        enabled: true
    # volumes:
    #   - name: background-image-1
    #     projected:
    #       defaultMode: 0775
    #       sources:
    #         - configMap:
    #             name: background-image-1
    #             items:
    #               - key: sound_wave.svg
    #                 path: sound_wave.svg
    #   - name: authentik-media
    #     persistentVolumeClaim:
    #       claimName: authentik-media
    # volumeMounts:
    #   - name: authentik-media
    #     mountPath: /media
    #     subPath: media
    #   - name: authentik-media
    #     mountPath: /templates
    #     subPath: custom-templates
    redis:
      enabled: false
      # auth:
      #   enabled: true
      # master:
      #   persistence:
      #     enabled: true
      #     size: 1Gi
      #   resources:
      #     requests:
      #       cpu: 15m
      #       memory: 50Mi
      #     limits:
      #       memory: 100Mi
      # commonConfiguration: |-
      #   # Enable AOF https://redis.io/topics/persistence#append-only-file
      #   appendonly yes
      #   # Disable RDB persistence, AOF persistence already enabled.
      #   save ""
      #   maxmemory 94371840
      #   maxmemory-policy allkeys-lru
      metrics:
        enabled: true
        serviceMonitor:
          enabled: true
        resources:
          requests:
            cpu: 10m
            memory: 10Mi
          limits:
            memory: 20Mi
    # autoscaling:
    #   server:
    #     enabled: true
    #   worker:
    #     enabled: true
    serviceAccount:
      # -- Service account is needed for managed outposts
      create: true
      annotations: {}
      serviceAccountSecret:
        # -- As we use the authentik-remote-cluster chart as subchart, and that chart
        # creates a service account secret by default which we don't need here, disable its creation
        enabled: false
      fullnameOverride: authentik
      nameOverride: authentik
    # The redis sub-chart does not have arm support
    # Authentik helm applies this to all it's pieces. No mix-and-match like with keycloak
    # May have to stop with the keycloak comparisons. Keycloak is far superior for customizability (imo)
    # volumes:
    #   - configMap:
    #       name: authentik-branding
    #     name: branding
    # volumeMounts:
    #   - mountPath: /media/branding
    #     name: branding

  valuesFrom:
    - kind: Secret
      name: *secret
      valuesKey: AUTHENTIK_SECRET_KEY
      targetPath: authentik.secret_key
    - kind: Secret
      name: *secret
      valuesKey: SMTP_HOST
      targetPath: authentik.email.host
    - kind: Secret
      name: *secret
      valuesKey: SMTP_USERNAME
      targetPath: authentik.email.username
    - kind: Secret
      name: *secret
      valuesKey: SMTP_PASS
      targetPath: authentik.email.password
    - kind: Secret
      name: *secret
      valuesKey: POSTGRES_HOST
      targetPath: authentik.postgresql.host
    # - kind: Secret
    #   name: *secret
    #   valuesKey: POSTGRES_PORT
    #   targetPath: authentik.postgresql.port
    - kind: Secret
      name: *secret
      valuesKey: POSTGRES_USER
      targetPath: authentik.postgresql.user
    - kind: Secret
      name: *secret
      valuesKey: POSTGRES_PASS
      targetPath: authentik.postgresql.password
    - kind: Secret
      name: *secret
      valuesKey: POSTGRES_DB
      targetPath: authentik.postgresql.name
    # - kind: Secret
    #   name: *secret
    #   valuesKey: AUTHENTIK_REDIS_URL
    #   targetPath: authentik.redis.host
    # - kind: Secret
    #   name: *secret
    #   valuesKey: REDIS_PASS
    #   targetPath: authentik.redis.password
    # - kind: Secret
    #   name: *secret
    #   valuesKey: REDIS_PASS
    #   targetPath: redis.auth.password
