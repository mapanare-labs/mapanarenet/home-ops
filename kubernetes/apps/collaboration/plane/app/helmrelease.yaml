---
apiVersion: helm.toolkit.fluxcd.io/v2
kind: HelmRelease
metadata:
  name: &app plane
spec:
  interval: 30m
  chart:
    spec:
      chart: app-template
      version: 3.6.1
      sourceRef:
        kind: HelmRepository
        name: bjw-s
        namespace: flux-system
  maxHistory: 2
  install:
    remediation:
      retries: 3
  upgrade:
    cleanupOnFail: true
    remediation:
      strategy: rollback
      retries: 3
  dependsOn:
    - name: dragonfly-operator
      namespace: databases
  values:
    controllers:
      web:
        annotations:
          reloader.stakater.com/auto: "true"
        initContainers:
          init-db:
            image:
              repository: ghcr.io/onedr0p/postgres-init
              tag: 16
            envFrom: &envFrom
              - secretRef:
                  name: twenty-secret
        containers:
          app:
            image:
              repository: docker.io/twentycrm/twenty
              tag: v0.32.0
            env:
              TZ: &tz America/Santiago
              SERVER_URL: &serverURL "https://crm.mapanare.net:443"
              FRONT_BASE_URL: &frontBaseURL "https://crm.mapanare.net:443"
              PORT: &port 3000
              LOG_LEVELS: log
              CACHE_STORAGE_TYPE: redis
              REDIS_URL: &redisURL redis://dragonfly.databases.svc.cluster.local:6379
              ENABLE_DB_MIGRATIONS: true
              SIGN_IN_PREFILLED: true
              STORAGE_TYPE: local
              MESSAGE_QUEUE_TYPE: bull-mq
              ACCESS_TOKEN_EXPIRES_IN: 7d
              LOGIN_TOKEN_EXPIRES_IN: 1h
              EMAIL_DRIVER: smtp
            envFrom: *envFrom
            resources:
              requests:
                cpu: 2500m
                memory: 256Mi
              limits:
                memory: 1024Mi
            probes:
              liveness: &probes
                enabled: true
                custom: true
                spec:
                  httpGet:
                    path: /healthz
                    port: *port
                  initialDelaySeconds: 0
                  periodSeconds: 5
                  timeoutSeconds: 5
                  failureThreshold: 10
              readiness: *probes
      worker:
        annotations:
          reloader.stakater.com/auto: "true"
        initContainers:
          init-db:
            image:
              repository: ghcr.io/onedr0p/postgres-init
              tag: 16
            envFrom: &envFrom
              - secretRef:
                  name: twenty-secret
        containers:
          app:
            image:
              repository: docker.io/twentycrm/twenty
              tag: v0.32.0
            env:
              TZ: *tz
              SERVER_URL: *serverURL
              FRONT_BASE_URL: *frontBaseURL
              PORT: *port
              LOG_LEVELS: log
              CACHE_STORAGE_TYPE: redis
              REDIS_URL: *redisURL
              ENABLE_DB_MIGRATIONS: false
              STORAGE_TYPE: local
              MESSAGE_QUEUE_TYPE: bull-mq
              EMAIL_DRIVER: smtp
            envFrom: *envFrom
            command:
              [
                "yarn",
                "worker:prod"
              ]
            resources:
              requests:
                cpu: 250m
                memory: 1024Mi
              limits:
                memory: 2048Mi
    service:
      app:
        controller: server
        ports:
          http:
            port: 3000
    ingress:
      app:
        className: internal
        hosts:
          - host: &host crm.mapanare.net
            paths:
              - path: /
                service:
                  identifier: app
                  port: http
        tls:
          - hosts:
              - *host
    persistence:
      docker-data:
        existingClaim: twenty-docker-data
        globalMounts:
          - path: /app/docker-data
      server-data:
        existingClaim: twenty-server-data
        globalMounts:
          - path: /app/packages/twenty-server/.local-storage