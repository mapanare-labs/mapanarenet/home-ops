module.exports = {
    delay: 20,
    qbittorrentUrl: "http://{{ .QB_USER }}:{{ .QB_PASS }}@qbittorrent.multimedia.svc.cluster.local",
    torznab: [
      "http://prowlarr.multimedia.svc.cluster.local/55/api?apikey={{ .PROWLARR__API_KEY }}",  // tpb --
      "http://prowlarr.multimedia.svc.cluster.local/53/api?apikey={{ .PROWLARR__API_KEY }}",  // kass --
      "http://prowlarr.multimedia.svc.cluster.local/60/api?apikey={{ .PROWLARR__API_KEY }}",  // tdown --
      "http://prowlarr.multimedia.svc.cluster.local/59/api?apikey={{ .PROWLARR__API_KEY }}",  // tz2eu --
      "http://prowlarr.multimedia.svc.cluster.local/58/api?apikey={{ .PROWLARR__API_KEY }}",  // LinuxTracker --
      "http://prowlarr.multimedia.svc.cluster.local/64/api?apikey={{ .PROWLARR__API_KEY }}",  // solidtz
      "http://prowlarr.multimedia.svc.cluster.local/51/api?apikey={{ .PROWLARR__API_KEY }}",  // gametz --
      "http://prowlarr.multimedia.svc.cluster.local/56/api?apikey={{ .PROWLARR__API_KEY }}",  // bass --
      "http://prowlarr.multimedia.svc.cluster.local/54/api?apikey={{ .PROWLARR__API_KEY }}",  // limetz --
      "http://prowlarr.multimedia.svc.cluster.local/45/api?apikey={{ .PROWLARR__API_KEY }}",  // tlck
      "http://prowlarr.multimedia.svc.cluster.local/50/api?apikey={{ .PROWLARR__API_KEY }}",  // 1337 --
      "http://prowlarr.multimedia.svc.cluster.local/61/api?apikey={{ .PROWLARR__API_KEY }}",  // ybt --
      "http://prowlarr.multimedia.svc.cluster.local/62/api?apikey={{ .PROWLARR__API_KEY }}",  // eztv --
      "http://prowlarr.multimedia.svc.cluster.local/63/api?apikey={{ .PROWLARR__API_KEY }}",  // tds --
      "http://prowlarr.multimedia.svc.cluster.local/57/api?apikey={{ .PROWLARR__API_KEY }}",  // intarch --
      "http://prowlarr.multimedia.svc.cluster.local/52/api?apikey={{ .PROWLARR__API_KEY }}",  // moviesDVDR --
    ],

    port: 80,
    action: "inject",
    includeEpisodes: false,
    includeSingleEpisodes: true,
    includeNonVideos: true,
    duplicateCategories: true,
    matchMode: "safe",
    skipRecheck: true,
    linkType: "hardlink",
    linkDir: "/media/Downloads/qbittorrent/complete/cross-seed",
    dataDirs: [
      "/media/Downloads/qbittorrent/complete/prowlarr",
      "/media/Downloads/qbittorrent/complete/radarr",
      "/media/Downloads/qbittorrent/complete/sonarr",
    ],
    maxDataDepth: 1,
    outputDir: "/config/xseeds",
    torrentDir: "/config/qBittorrent/BT_backup",
  };