AUTHENTICATION_SOURCES = ['oauth2', 'internal']
OAUTH2_AUTO_CREATE_USER = True
OAUTH2_CONFIG = [{
    'OAUTH2_NAME' : 'authentik',
    'OAUTH2_DISPLAY_NAME' : 'Authentik',
    'OAUTH2_CLIENT_ID' : '{{ .OAUTH2_CLIENT_ID }}',
    'OAUTH2_CLIENT_SECRET' : '{{ .OAUTH2_CLIENT_SECRET }}',
    'OAUTH2_TOKEN_URL' : 'https://sso.mapanare.net/application/o/token/',
    'OAUTH2_AUTHORIZATION_URL' : 'https://sso.mapanare.net/application/o/authorize/',
    'OAUTH2_API_BASE_URL' : 'https://sso.mapanare.net/',
    'OAUTH2_USERINFO_ENDPOINT' : 'https://sso.mapanare.net/application/o/userinfo/',
    'OAUTH2_SERVER_METADATA_URL' : 'https://sso.mapanare.net/application/o/pgadmin/.well-known/openid-configuration',
    'OAUTH2_SCOPE' : 'openid email profile',
    'OAUTH2_ICON' : 'fa-solid fa-users',
    'OAUTH2_BUTTON_COLOR' : '<button-color>'
}]