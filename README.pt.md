# HomeOps

<div align="center">

<img src="https://camo.githubusercontent.com/5b298bf6b0596795602bd771c5bddbb963e83e0f/68747470733a2f2f692e696d6775722e636f6d2f7031527a586a512e706e67" align="center" width="144px" height="144px"/>

### Repositorio de mi homelab :octocat:

_... gestionado con Flux, GitLab CI_ 🤖

</div>

<div align="center">

[![Kubernetes](https://img.shields.io/badge/v1.26-blue?style=for-the-badge&logo=kubernetes&logoColor=white)](https://k3s.io/)&nbsp;&nbsp;&nbsp;

[![Status-Page](https://img.shields.io/uptimerobot/status/m793599155-ba1b18e51c9f8653acd0f5c1?color=brightgreeen&label=Status%20Page&style=for-the-badge&logo=statuspage&logoColor=white)](https://status.mapanare.net)&nbsp;&nbsp;&nbsp;
</div>

---

## Languages / Idiomas

Also in: [English](https://gitlab.com/mapanare-labs/mapanarenet/home-ops/-/blob/main/README.en.md)  

Também em: [Portugués](https://gitlab.com/mapanare-labs/mapanarenet/home-ops/-/blob/main/README.pt.md)

---

## 📖 Descripción

Este es un mono repositorio donde gestiono las recetas de mi infraestructura casera y mi cluster de Kubernetes. Intento que se adhiera a las buenas practicas de  Infraestructura como Código (IaC) y GitOps usando herramientas como [Ansible](https://www.ansible.com/), [Terraform](https://www.terraform.io/), [Kubernetes](https://kubernetes.io/), [Flux](https://github.com/fluxcd/flux2), y [GitLab CI](https://docs.gitlab.com/ee/ci/).

---

## ⛵ Kubernetes

### Instalación

Mi clúster [k3s](https://k3s.io/) ha sido aprovisionado en máquinas virtuales con Ubuntu 22.04 usando la colección de [Ansible](https://www.ansible.com/) [k3s](https://galaxy.ansible.com/enmanuelmoreira/k3s). This is a semi hyper-converged cluster, workloads and block storage are sharing the same available resources on my nodes while I have a separate server for (NFS) file storage.

🔸 _[Haz click aquí](./ansible/) para ver algunos playbooks y roles que utilizo además de k3s._

### Componentes Principales

- [gitlab-runner-controller](https://docs.gitlab.com/runner/executors/kubernetes.html): self-hosted GitLab runners
- [cert-manager](https://cert-manager.io/docs/): crea certificados SSL para los servicios de mi clúster
- [external-secrets](https://github.com/external-secrets/external-secrets/): uso  [Doppler](https://github.com/DopplerHQ) para gestionar los secretos de los diferentes deployments del clúster.
- [ingress-nginx](https://github.com/kubernetes/ingress-nginx/): controlador ingress de Kubernetes usando NGINX como proxy reverso y balanceador de carga
- [rook](https://github.com/rook/rook): almacenamiento en bloques distribuido para almacenamiento persistente
- [sops](https://toolkit.fluxcd.io/guides/mozilla-sops/): gestión de secretos para  Kubernetes, Ansible y Terraform los cuales son enviados via Git

### GitOps

[Flux](https://github.com/fluxcd/flux2) monitoriza mi directorio [kubernetes](./kubernetes/) (ver Directorios del repositorio más abajo) y realiza los cambios en mi mi cluster basado en los manifiestos YAML.

La manera en que Flux funciona para mi es que busca recursivamente en los directorios [kubernetes/apps](./kubernetes/apps) hasta que encuentra el archivo `kustomization.yaml` por cada directorio y luego aplica todos los cambios a los recursos listados dentro de el. El mencionado archivo `kustomization.yaml` puede contener generalmente solo un recurso `namespace` o también uno o más kustomization de Flux. Estas kustomizations de Flux tienen un recurso `HelmRelease` o cualquier otro recurso relacionado con la aplicación a la cual se aplicarán los cambios.

[Renovate](https://github.com/renovatebot/renovate) monitoriza mi repositorio **entero** buscando por las versiones más recientes de todas las dependencias, y si las hay, crea un MR automaticamente. Cuando algunos MR son mergeados, [Flux](https://github.com/fluxcd/flux2) aplica los cambios en mi cluster.

### Directorios

Este repositorio  Git contiene los siguientes directorios bajo [kubernetes](./kubernetes/).

```sh
📁 kubernetes      # Kubernetes cluster definido como codigo
├─📁 bootstrap     # Instalacion de Flux
├─📁 flux          # Configuracion principal de Flux
└─📁 apps          # Apps desplegadas en mi cluster agrupadas por namespace
```

---

## Flux (Instalación y Configuración)

### Crear Namespace flux-system

```bash
kubectl create ns flux-system
```

### Age

#### Crear llave publica y privada de age

```bash
age-keygen -o ~/.age/key.txt
```

```bash
kubectl -n flux-system create secret generic sops-age \
  --from-file=age.agekey=/home/user/.age/key.txt
```

### GitLab

#### Crear Llave SSH para Flux

```bash
ssh-keygen -f ~/.ssh/fluxcd -P ""
```

#### Extrayendo la Clave known_host de GitLab

```bash
export GL_SSH=$(ssh-keyscan gitlab.com) 
```

### Creando secreto para Flux

Creamos el secreto con la llave publica y privada SSH y el valor de know_hosts para que Flux detecte los cambios en el repositorio:  

```bash
kubectl create secret generic gitlab-deploy-key --from-file=identity=/home/user/.ssh/fluxcd --from-file=identity.pub=/home/user/.ssh/fluxcd.pub --from-literal=known_hosts=$GL_SSH --namespace flux-system
```

### Iniciando Flux

```bash
kubectl apply --server-side --kustomize ./kubernetes/bootstrap/flux
```

### Aplicando la Configuración del Cluster

_No se puede aplicar con el comando `kubectl` directamente, sino que tenemos que desencriptar el secreto con sops y luego aplicar el manifiesto_

```bash
sops --decrypt kubernetes/bootstrap/flux/age-key.sops.yaml | kubectl apply -f -
```

```bash
sops --decrypt kubernetes/bootstrap/flux/gitlab-deploy-key.sops.yaml | kubectl apply -f -
```

```bash
sops --decrypt kubernetes/flux/vars/cluster-secrets.sops.yaml | kubectl apply -f -
```

```bash
kubectl apply -f kubernetes/flux/vars/cluster-settings.yaml
```

### Aplicando la Configuración de Flux

```bash
kubectl apply --server-side --kustomize ./kubernetes/flux/config
```

---

## 📜 Historial de Cambios

Ve mi _horrible_ [historial de cambios](https://gitlab.com/mapanare-labs/mapanarenet/home-ops/-/commits/main?ref_type=heads)

---

## 🤝 Agradecimientos

- [billimek/k8s-gitops](https://github.com/billimek/k8s-gitops)
- [carpenike/k8s-gitops](https://github.com/carpenike/k8s-gitops)
- [onedr0p/home-ops](https://github.com/onedr0p/home-ops)
- [buroa/k8s-gitops](https://github.com/buroa/k8s-gitops)
- [lenaxia/home-ops-dev](https://github.com/lenaxia/home-ops-dev)
- [bjw-s/home-ops](https://github.com/bjw-s/home-ops)

---
