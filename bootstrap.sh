#!/bin/bash

doppler secrets download --no-file --format json  jq -r '. | to_entries[] | select(.key | startswith("TALOS")) | { (.key): (.value)}' | jq -s add | doppler run -p $DOPPLER_PROJECT -c $DOPPLER_ENV -- talhelper genconfig -c ./kubernetes/bootstrap/talos/talconfig.yaml -o ./kubernetes/bootstrap/talos/clusterconfig


doppler secrets download --no-file --format json | \
jq -r '. | to_entries[] | select(.key | startswith("TALOS") or startswith("DOCKERHUB")) | { (.key): (.value)}' | \
jq -s add | \
doppler run -p $DOPPLER_PROJECT -c $DOPPLER_ENV -- talhelper genconfig -c ./kubernetes/bootstrap/talos/talconfig.yaml -o ./kubernetes/bootstrap/talos/clusterconfig

talosctl apply-config --nodes=192.168.1.40 --file=./kubernetes/bootstrap/talos/clusterconfig/main-k8s-master-prdnpci-0.yaml --insecure
talosctl apply-config --nodes=192.168.1.41 --file=./kubernetes/bootstrap/talos/clusterconfig/main-k8s-master-prdnpci-1.yaml --insecure
talosctl apply-config --nodes=192.168.1.42 --file=./kubernetes/bootstrap/talos/clusterconfig/main-k8s-master-prdnpci-2.yaml --insecure
talosctl apply-config --nodes=192.168.1.50 --file=./kubernetes/bootstrap/talos/clusterconfig/main-k8s-worker-prdnpci-0.yaml --insecure
talosctl apply-config --nodes=192.168.1.51 --file=./kubernetes/bootstrap/talos/clusterconfig/main-k8s-worker-prdnpci-1.yaml --insecure
talosctl apply-config --nodes=192.168.1.52 --file=./kubernetes/bootstrap/talos/clusterconfig/main-k8s-worker-prdnpci-2.yaml --insecure
talosctl apply-config --nodes=192.168.1.54 --file=./kubernetes/bootstrap/talos/clusterconfig/main-k8s-worker-prdnpci-4.yaml --insecure
sleep 360

talosctl bootstrap --nodes=192.168.1.40 --talosconfig ./kubernetes/bootstrap/talos/clusterconfig/talosconfig
sleep 180

talosctl kubeconfig $HOME/.kube/config -n 192.168.1.40 --talosconfig ./kubernetes/bootstrap/talos/clusterconfig/talosconfig -f

helmfile apply -f ./kubernetes/bootstrap/helmfile.yaml

doppler secrets -p $DOPPLER_PROJECT -c $DOPPLER_ENV substitute ./kubernetes/bootstrap/apps/templates/resources.yaml.j2 | kubectl apply --server-side --filename -
