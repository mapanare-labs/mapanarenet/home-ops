# Ubuntu Server Noble
# ---
# Packer Template to create an Ubuntu Server (noble) on Proxmox

# Required plugins

packer {
  required_version = ">= 1.11.2"
  required_plugins {
    proxmox = {
      version = " >= 1.2.1"
      source = "github.com/hashicorp/proxmox"
    }
  }
}

# Variable Definitions
variable "proxmox_api_url" {
  type    = string
  default = ""
  sensitive = true
}

variable "proxmox_api_token_id" {
  type    = string
  default = ""
  sensitive = true
}

variable "proxmox_api_token_secret" {
  type      = string
  default   = ""
  sensitive = true
}

variable "proxmox_nodes" {
  type    = string
  default = "mamba"
}

variable "proxmox_vm_id" {
  type    = string
  default = "9004"
}

variable "ubuntu_version" {
  type      = string
  default   = "24.04.1"
}

# Resource Definition for the VM Template
source "proxmox-iso" "ubuntu-server-noble" {
  # Proxmox Connection Settings
  proxmox_url = "${var.proxmox_api_url}"
  username    = "${var.proxmox_api_token_id}"
  token       = "${var.proxmox_api_token_secret}"
  # (Optional) Skip TLS Verification
  insecure_skip_tls_verify = true

  # VM General Settings
  node                 = var.proxmox_nodes
  vm_id                = var.proxmox_vm_id
  vm_name              = "ubuntu-2404-cloudinit-template"
  template_description = "Ubuntu ${var.ubuntu_version} version built with Packer"
  #tags                 = "template;vm;ubuntu;22.04;lts"

  # VM OS Settings
  # (Option 1) Local ISO File
  iso_file = "nas-iso:iso/ubuntu-${var.ubuntu_version}-live-server-amd64.iso"
  # - or -
  # (Option 2) Download ISO
  # iso_url      = "https://releases.ubuntu.com/24.04.1/ubuntu-24.04.1-live-server-amd64.iso"
  # iso_checksum = "10f19c5b2b8d6db711582e0e27f5116296c34fe4b313ba45f9b201a5007056cb"
  iso_storage_pool = "nas-iso"
  unmount_iso      = true

  # VM System Settings
  qemu_agent = true

  # VM Hard Disk Settings
  scsi_controller = "virtio-scsi-pci"

  disks {
    disk_size         = "20G"
    format            = "raw"
    storage_pool      = "local-lvm"
    storage_pool_type = "lvm"
    type              = "virtio"
  }

  os   = "l26"  
  bios = "seabios"

  # VM CPU Settings
  cpu_type    = "host"
  cores       = "2"

  # VM Memory Settings
  memory = "2048"

  # VM Network Settings
  network_adapters {
    model    = "virtio"
    bridge   = "vmbr0"
    firewall = "false"
  }

  # VM Cloud-Init Settings
  cloud_init              = true
  cloud_init_storage_pool = "local-lvm"

  # PACKER Boot Commands
  boot_command = [
    "<spacebar><wait><spacebar><wait><spacebar><wait><spacebar><wait><spacebar><wait>",
    "e<wait>",
    "<down><down><down><end>",
    " autoinstall ds=nocloud-net\\;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/",
    "<f10>"
  ]
  boot      = "c"
  boot_wait = "5s"


  # PACKER Autoinstall Settings
  http_directory    = "http"
  http_bind_address = "192.168.0.137"
  http_port_min     = 8802
  http_port_max     = 8802

  ssh_username = "ubuntu"

  # (Option 1) Add your Password here
  # ssh_password = ""
  # - or -
  # (Option 2) Add your Private SSH KEY file here
  ssh_private_key_file = "~/.ssh/packer"

  # Communicator Settings
  # communicator  = "ssh"
  # ssh_port      = 22
  
  # Raise the timeout, when installation takes longer
  ssh_timeout            = "30m"
  ssh_handshake_attempts = 1000
}

# Build Definition to create the VM Template
build {
  name    = "ubuntu-server-noble"
  sources = ["source.proxmox-iso.ubuntu-server-noble"]

  # Provisioning the VM Template for Cloud-Init Integration in Proxmox #1
  provisioner "shell" {
    inline = [
      "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for cloud-init...'; sleep 1; done",
      "sudo rm /etc/ssh/ssh_host_*",
      "sudo truncate -s 0 /etc/machine-id",
      "sudo apt -y autoremove --purge",
      "sudo apt -y clean",
      "sudo apt -y autoclean",
      "sudo cloud-init clean",
      "sudo rm -f /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg",
      "sudo sync"
    ]
  }

  # Provisioning the VM Template for Cloud-Init Integration in Proxmox #2
  provisioner "file" {
    source      = "files/99-pve.cfg"
    destination = "/tmp/99-pve.cfg"
  }

  # Provisioning the VM Template for Cloud-Init Integration in Proxmox #3
  provisioner "shell" {
    inline = ["sudo cp /tmp/99-pve.cfg /etc/cloud/cloud.cfg.d/99-pve.cfg"]
  }

  # Add additional provisioning scripts here
  # ...
}
