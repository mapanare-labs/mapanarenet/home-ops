# Packer Builder

Create a packer group:

```bash
pveum groupadd packer
```

Create a PackerBuiler role with the following permissions:

```bash
pveum roleadd PackerBuilder -privs "VM.Config.Disk VM.Config.CPU VM.Config.Memory VM.Config.Cloudinit Datastore.AllocateSpace Sys.Modify VM.Config.Options VM.Allocate VM.Audit VM.Console VM.Config.CDROM VM.Config.Network VM.PowerMgmt VM.Config.HWType VM.Monitor"
```

Assign PackerBuilder Role to group packer:

```bash
pveum aclmod / -group packer -role PackerBuilder
```

Create user packer and assign it to group packer:

```bash
pveum user add packer@pve -group packer
```

Create a API Token:

```bash
pveum user token add packer@pve packer -privsep 0
```
