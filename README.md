# HomeOps

<div align="center">

<img src="https://gitlab.com/mapanare-labs/mapanarenet/home-ops/-/raw/main/docs/src/assets/logo.png" align="center" width="144px" height="144px"/>

### Repositorio de mi homelab :octocat:

_... gestionado con Flux, GitLab CI_ 🤖

</div>

<div align="center">

[![Talos](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Ftalos_version&style=for-the-badge&logo=talos&logoColor=white&color=blue&label=%20)](https://talos.dev)&nbsp;&nbsp;
[![Kubernetes](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Fkubernetes_version&style=for-the-badge&logo=kubernetes&logoColor=white&color=blue&label=%20)](https://kubernetes.io)&nbsp;&nbsp;
[![Flux](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Fflux_version&style=for-the-badge&logo=flux&logoColor=white&color=blue&label=%20)](https://fluxcd.io)&nbsp;&nbsp;
[![Renovate](https://img.shields.io/github/actions/workflow/status/onedr0p/home-ops/renovate.yaml?branch=main&label=&logo=renovatebot&style=for-the-badge&color=blue)](https://github.com/onedr0p/home-ops/actions/workflows/renovate.yaml)

</div>

<div align="center">

[![Home-Internet](https://img.shields.io/uptimerobot/status/m793494864-dfc695db066960233ac70f45?color=brightgreeen&label=Home%20Internet&style=for-the-badge&logo=ubiquiti&logoColor=white)](https://status.mapanare.net)&nbsp;&nbsp;
[![Status-Page](https://img.shields.io/uptimerobot/status/m793599155-ba1b18e51c9f8653acd0f5c1?color=brightgreeen&label=Status%20Page&style=for-the-badge&logo=statuspage&logoColor=white)](https://status.mapanare.net)&nbsp;&nbsp;
[![Alertmanager](https://img.shields.io/uptimerobot/status/m793494864-dfc695db066960233ac70f45?color=brightgreeen&label=Alertmanager&style=for-the-badge&logo=prometheus&logoColor=white)](https://status.mapanare.net)

</div>

<div align="center">

[![Age-Days](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Fcluster_age_days&style=flat-square&label=Age)](https://github.com/kashalls/kromgo)&nbsp;&nbsp;
[![Uptime-Days](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Fcluster_uptime_days&style=flat-square&label=Uptime)](https://github.com/kashalls/kromgo)&nbsp;&nbsp;
[![Node-Count](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Fcluster_node_count&style=flat-square&label=Nodes)](https://github.com/kashalls/kromgo)&nbsp;&nbsp;
[![Pod-Count](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Fcluster_pod_count&style=flat-square&label=Pods)](https://github.com/kashalls/kromgo)&nbsp;&nbsp;
[![CPU-Usage](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Fcluster_cpu_usage&style=flat-square&label=CPU)](https://github.com/kashalls/kromgo)&nbsp;&nbsp;
[![Memory-Usage](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Fcluster_memory_usage&style=flat-square&label=Memory)](https://github.com/kashalls/kromgo)&nbsp;&nbsp;
[![Power-Usage](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Fcluster_power_usage&style=flat-square&label=Power)](https://github.com/kashalls/kromgo)&nbsp;&nbsp;
[![Alerts](https://img.shields.io/endpoint?url=https%3A%2F%2Fkromgo.mapanare.net%2Fcluster_alert_count&style=flat-square&label=Alerts)](https://github.com/kashalls/kromgo)

</div>

---

## Languages / Idiomas

Also in:  [English](https://gitlab.com/mapanare-labs/mapanarenet/home-ops/-/blob/main/README.en.md)  

Também em:  [Portugués](https://gitlab.com/mapanare-labs/mapanarenet/home-ops/-/blob/main/README.pt.md)

---

## <img src="https://fonts.gstatic.com/s/e/notoemoji/latest/270f_fe0f/512.gif" alt="✏" width="20" height="20"> Descripción

Este es un mono repositorio donde gestiono las recetas de mi infraestructura casera y mi cluster de Kubernetes. Intento que se adhiera a las buenas practicas de  Infraestructura como Código (IaC) y GitOps usando herramientas como [Ansible](https://www.ansible.com/), [Terraform](https://www.terraform.io/), [Kubernetes](https://kubernetes.io/), [Flux](https://github.com/fluxcd/flux2), y [GitLab CI](https://docs.gitlab.com/ee/ci/).

---

## <img src="https://fonts.gstatic.com/s/e/notoemoji/latest/1f6f8/512.gif" alt="🛸" width="20" height="20"> Kubernetes

### Instalación

Mi clúster [k8s](https://kubernetes.io/) ha sido aprovisionado en máquinas virtuales con [Talos Linux](https://talos.dev/). Este es un clúster semi hiperconvergente, las cargas de trabajo y el almacenamiento en bloque comparten los mismos recursos disponibles en mis nodos, mientras que tengo un servidor separado para el almacenamiento de archivos (NFS).

### Componentes Principales

- [gitlab-runner-controller](https://docs.gitlab.com/runner/executors/kubernetes.html): self-hosted GitLab runners.
- [cert-manager](https://cert-manager.io/docs/): crea certificados SSL para los servicios de mi clúster.
- [external-secrets](https://github.com/external-secrets/external-secrets/): uso  [Doppler](https://github.com/DopplerHQ) para gestionar los secretos de los diferentes deployments del clúster.
- [ingress-nginx](https://github.com/kubernetes/ingress-nginx/): controlador ingress de Kubernetes usando NGINX como proxy reverso y balanceador de carga.
- [rook](https://github.com/rook/rook): almacenamiento en bloques distribuido para almacenamiento persistente.
- [sops](https://toolkit.fluxcd.io/guides/mozilla-sops/): gestión de secretos para  Kubernetes, Ansible y Terraform los cuales son enviados via Git.

### GitOps

[Flux](https://github.com/fluxcd/flux2) monitoriza mi directorio [kubernetes](./kubernetes/) (ver Directorios del repositorio más abajo) y realiza los cambios en mi mi cluster basado en los manifiestos YAML.

La manera en que Flux funciona para mi es que busca recursivamente en los directorios [kubernetes/apps](./kubernetes/apps) hasta que encuentra el archivo `kustomization.yaml` por cada directorio y luego aplica todos los cambios a los recursos listados dentro de el. El mencionado archivo `kustomization.yaml` puede contener generalmente solo un recurso `namespace` o también uno o más kustomization de Flux. Estas kustomizations de Flux tienen un recurso `HelmRelease` o cualquier otro recurso relacionado con la aplicación a la cual se aplicarán los cambios.

[Renovate](https://github.com/renovatebot/renovate) monitoriza mi repositorio **entero** buscando por las versiones más recientes de todas las dependencias, y si las hay, crea un MR automaticamente. Cuando algunos MR son mergeados, [Flux](https://github.com/fluxcd/flux2) aplica los cambios en mi cluster.

### Directorios

Este repositorio  Git contiene los siguientes directorios bajo [kubernetes](./kubernetes/).

```sh
📁 kubernetes       # Kubernetes cluster definido como codigo
├─📁 apps           # Apps desplegadas en mi cluster agrupadas por namespace
├─📁 bootstrap      # Procesos de instalación
├─📁 crds           # Instalación de crds varios
└─📁 flux           # Configuración principal de Flux y componentes reusables  

```

### Flujo de Trabajo de Flux

A grandes rasgos, en el siguiente gráfico se ve como Flux despliega mis aplicaciones con sus respectivas dependencias. Debajo de las 3 aplicaciones llamadas `postgres`, `lldap` y `authelia`. `postgres` es la primera aplicación que debe estar en ejecución antes que `lldap` y `authelia`. Una vez que `postgres` se encuentre en estado "healthy", `lldap` será desplegado y después lo hará `authelia`.

```mermaid
graph TD
    A>Kustomization: rook-ceph] -->|Crea| B[HelmRelease: rook-ceph]
    A>Kustomization: rook-ceph] -->|Crea| C[HelmRelease: rook-ceph-cluster]
    C>HelmRelease: rook-ceph-cluster] -->|Depende de| B>HelmRelease: rook-ceph]
    D>Kustomization: atuin] -->|Crea| E(HelmRelease: atuin)
    E>HelmRelease: atuin] -->|Depende de| C>HelmRelease: rook-ceph-cluster]
```

---

## <img src="https://fonts.gstatic.com/s/e/notoemoji/latest/1f636_200d_1f32b_fe0f/512.gif" alt="😶" width="20" height="20"> Recursos en la Nube

Si bien la mayor parte de mi infraestructura y cargas de trabajo son autohospedadas, confío en la nube para ciertas partes clave de mi configuración. Esto me ahorra tener que preocuparme por tres cosas. (1) Lidiar con escenarios de quien fue primero, el huevo o la gallina, (2) servicios que necesito de manera crítica ya sea que mi clúster esté en línea o no y (3) El "factor de impacto de bus": qué sucede con las aplicaciones críticas (por ejemplo, correo electrónico, administrador de contraseñas, fotos)

Las soluciones alternativas a los dos primeros problemas serían desplegar un clúster de Kubernetes en la nube e implementar aplicaciones como [HCVault](https://www.vaultproject.io/), [Vaultwarden](<https://github.com> /dani-garcia/vaultwarden), [ntfy](https://ntfy.sh/) y [Gatus](https://gatus.io/); sin embargo, mantener otro cluster y monitorear otro grupo de cargas de trabajo supondría más trabajo y probablemente tendría más o menos los mismos costos que se describen a continuación.

| Servicio                                         | Uso                                                               | Costo           |
|-------------------------------------------------|-------------------------------------------------------------------|----------------|
| [Doppler](https://doppler.com/)             | Almacenamiento de Secretos con el servicio [External Secrets](https://external-secrets.io/)     | Gratis        |
| [Cloudflare](https://www.cloudflare.com/)       | Dominio and S3                                                     | ~$30/yr        |
| [GitLab](https://gitlab.com/)                   | Hosting de este repositorio e integración continua/despliegue continuo    | Gratis           |
| [Migadu](https://migadu.com/)                   | Email hosting                                                     | ~$20/yr        |
| [Pushover](https://pushover.net/)               | Alertas de Kubernetes y Notificación de aplicaicones                   | $5 OTP         |
|                                                 |                                                                   | Total: ~$7/mes |

---

## <img src="https://fonts.gstatic.com/s/e/notoemoji/latest/1f30e/512.gif" alt="🌎" width="20" height="20"> DNS

En mi clúster hay dos instancias de [ExternalDNS](https://github.com/kubernetes-sigs/external-dns) ejecutándose. Una para sincronizar registros DNS privados con mi router `OPNSense` usando [proveedor de webhook DNS externo para OPNSense](https://github.com/kashalls/external-dns-unifi-webhook), mientras que otra instancia sincroniza DNS público con `Cloudflare`. Esta configuración se gestiona creando ingress con dos clases específicas: "interno" para DNS privado y "externo" para DNS público. Las instancias `external-dns` luego sincronizan los registros DNS con sus respectivas plataformas en consecuencia.

---

## <img src="https://fonts.gstatic.com/s/e/notoemoji/latest/2699_fe0f/512.gif" alt="⚙" width="20" height="20"> Hardware

<details>
  <summary>Click here to see my server rack</summary>

  <img src="https://raw.githubusercontent.com/onedr0p/home-ops/main/docs/src/assets/rack.png" align="center" width="200px" alt="dns"/>
</details>

| Dispositivo                      | Num | Tamaño Disco SO  | Tamaño Disco de Datos                  | Ram  | SO            | Función                |
|-----------------------------|-----|--------------|---------------------------------|------|---------------|-------------------------|
| Supermicro X10DRL-i Intel C612 Chipset Dual Xeon LGA2011-3 DDR4 ATX Motherboard   | 1   | 1TB SSD      | 1TB (local) / 800GB (rook-ceph) | 128GB | Proxmox 8.3         | Hypervisor              |
| Lenovo Thincentre M910Q   | 2   | 1TB SSD      | 1TB (local) / 800GB (rook-ceph) | 32GB | Proxmox 8.3         | Hypervisor  |
| Orange Pi 5 Plus   | 1   | 128GB eMMC      | 1TB (local) / 800GB (rook-ceph) | 32GB | Talos         | k8s-Worker  |
| SuperMicro X10SDV-4C-TLN2F Server Motherboard, Mini-ITX,Xeon D-1521              | 1   | 1TB SSD      | 3x4TB ZFS (mirrored vdevs)     | 128GB | TrueNAS SCALE | NFS + Servidor para Respaldos     |
| PiKVM (RasPi 4)             | 1   | 64GB (SD)    | -                               | 4GB  | PiKVM         | KVM                     |
| TESmart 8 Port KVM Switch   | 1   | -            | -                               | -    | -             | Network KVM (for PiKVM) |
| Lenovo Thincentre M910Q   | 1   | 1TB SSD      | 1TB (local) | 16GB | OPNSense         | Router  |
| TP-Link TL-SG1016DE             | 1   | -            | -                               | -    | -             | 1Gb Core Switch        |

## Flux (Instalación y Configuración)

### Crear Namespace flux-system

```bash
kubectl create ns flux-system
```

### Age

#### Crear llave publica y privada de age

```bash
age-keygen -o ~/.age/key.txt
```

```bash
kubectl -n flux-system create secret generic sops-age \
  --from-file=age.agekey=$HOME/.age/key.txt
```

### GitLab

#### Crear Llave SSH para Flux

```bash
ssh-keygen -f ~/.ssh/fluxcd -P ""
```

#### Extrayendo la Clave known_host de GitLab

```bash
export GL_SSH=$(ssh-keyscan gitlab.com) 
```

### Creando secreto para Flux

Creamos el secreto con la llave publica y privada SSH y el valor de know_hosts para que Flux detecte los cambios en el repositorio:  

```bash
kubectl create secret generic gitlab-deploy-key --from-file=identity=$HOME/.ssh/fluxcd --from-file=identity.pub=$HOME/.ssh/fluxcd.pub --from-literal=known_hosts=$GL_SSH --namespace flux-system
```

### Iniciando Flux

```bash
kubectl apply --server-side --kustomize ./kubernetes/bootstrap/flux
```

### Aplicando la Configuración del Cluster

_No se puede aplicar con el comando `kubectl` directamente, sino que tenemos que desencriptar el secreto con sops y luego aplicar el manifiesto_

```bash
sops --decrypt kubernetes/bootstrap/flux/age-keys.sops.yaml | kubectl apply -f -
```

```bash
sops --decrypt kubernetes/bootstrap/flux/gitlab-deploy-key.sops.yaml | kubectl apply -f -
```

```bash
sops --decrypt kubernetes/flux/vars/cluster-secrets.sops.yaml | kubectl apply -f -
```

```bash
kubectl apply -f kubernetes/flux/vars/cluster-settings.yaml
```

### Aplicando la Configuración de Flux

```bash
kubectl apply --server-side --kustomize ./kubernetes/flux/config
```

---

## 📜 Historial de Cambios

Ve mi _horrible_ [historial de cambios](https://gitlab.com/mapanare-labs/mapanarenet/home-ops/-/commits/main?ref_type=heads)

---

## <img src="https://fonts.gstatic.com/s/e/notoemoji/latest/1f64f/512.gif" alt="🙏" width="20" height="20"> Agradecimientos

- [billimek/k8s-gitops](https://github.com/billimek/k8s-gitops)
- [carpenike/k8s-gitops](https://github.com/carpenike/k8s-gitops)
- [onedr0p/home-ops](https://github.com/onedr0p/home-ops)
- [buroa/k8s-gitops](https://github.com/buroa/k8s-gitops)
- [lenaxia/home-ops-dev](https://github.com/lenaxia/home-ops-dev)
- [bjw-s/home-ops](https://github.com/bjw-s/home-ops)

---
