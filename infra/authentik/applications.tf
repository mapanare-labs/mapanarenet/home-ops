module "proxy-prowlarr" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/proxy-application.git?ref=main"
  name               = "Prowlarr"
  description        = "Torrent indexer"
  icon_url           = "https://raw.githubusercontent.com/Prowlarr/Prowlarr/develop/Logo/128.png"
  group              = "Downloads"
  slug               = "prowlarr"
  domain             = local.domain
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  auth_groups        = [authentik_group.media.id]
}

module "proxy-radarr" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/proxy-application.git?ref=main"
  name               = "Radarr"
  description        = "Movies"
  icon_url           = "https://github.com/Radarr/Radarr/raw/develop/Logo/128.png"
  group              = "Downloads"
  slug               = "radarr"
  domain             = local.domain
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  auth_groups        = [authentik_group.media.id]
}

module "proxy-sonarr" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/proxy-application.git?ref=main"
  name               = "Sonarr"
  description        = "TV"
  icon_url           = "https://github.com/Sonarr/Sonarr/raw/develop/Logo/128.png"
  group              = "Downloads"
  slug               = "sonarr"
  domain             = local.domain
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  auth_groups        = [authentik_group.media.id]
}

module "proxy-lidarr" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/proxy-application.git?ref=main"
  name               = "Lidarr"
  description        = "Music"
  icon_url           = "https://github.com/Lidarr/Lidarr/raw/develop/Logo/128.png"
  group              = "Downloads"
  slug               = "lidarr"
  domain             = local.domain
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  auth_groups        = [authentik_group.media.id]
}

module "proxy-bazarr" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/proxy-application.git?ref=main"
  name               = "Bazarr"
  description        = "Subtitles"
  icon_url           = "https://github.com/morpheus65535/bazarr/raw/master/frontend/public/images/logo128.png"
  group              = "Downloads"
  slug               = "bazarr"
  domain             = local.domain
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  auth_groups        = [authentik_group.media.id]
}

module "proxy-navidrome" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/proxy-application.git?ref=main"
  name               = "Navidrome"
  description        = "Music player"
  icon_url           = "https://github.com/navidrome/navidrome/raw/master/resources/logo-192x192.png"
  group              = "Media"
  slug               = "navidrome"
  domain             = local.domain
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  auth_groups        = [authentik_group.media.id]
  ignore_paths       = <<-EOT
  /rest/*
  /share/*
  EOT
}

module "proxy-internal-uptimekuma" {
  source                       = "git@gitlab.com:mapanare-labs/tm/authentik/proxy-internal-application.git?ref=main"
  name                         = "Uptimekuma"
  description                  = "Observability Tool"
  icon_url                     = "https://uptime.kuma.pet/img/icon.svg"
  group                        = "Infrastructure"
  slug                         = "uptime"
  domain                       = local.domain
  internal_host                = "http://uptimekuma.observability.svc.cluster.local:3001"
  internal_host_ssl_validation = false
  authorization_flow           = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow            = resource.authentik_flow.provider-invalidation.uuid
  auth_groups                  = [authentik_group.infrastructure.id]
  ignore_paths                 = <<-EOT
  ^/$
  ^/status
  ^/assets/
  ^/assets
  ^/icon.svg
  ^/api/.*
  ^/upload/.*
  ^/metrics
  EOT
}

module "oauth2-forgejo" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name               = "Forgejo"
  icon_url           = "https://codeberg.org/forgejo/forgejo/raw/branch/forgejo/assets/logo.svg"
  launch_url         = "https://git.${local.domain}"
  description        = "Git"
  newtab             = true
  group              = "Infrastructure"
  auth_groups        = [authentik_group.infrastructure.id]
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  client_id          = local.oauth_client_id["forgejo"]
  client_secret      = local.oauth_client_secret["forgejo"]
  redirect_uris      = ["https://git.${local.domain}/user/oauth2/authentik/callback"]
}

module "oauth2-grafana" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name               = "Grafana"
  icon_url           = "https://raw.githubusercontent.com/grafana/grafana/main/public/img/icons/mono/grafana.svg"
  launch_url         = "https://grafana.${local.domain}"
  description        = "Infrastructure graphs"
  newtab             = true
  group              = "Infrastructure"
  auth_groups        = [authentik_group.infrastructure.id]
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  client_id          = local.oauth_client_id["grafana"]
  client_secret      = local.oauth_client_secret["grafana"]
  redirect_uris      = ["https://grafana.${local.domain}/login/generic_oauth"]
}

module "oauth2-harbor" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name               = "Harbor"
  icon_url           = "https://raw.githubusercontent.com/goharbor/harbor/main/docs/img/harbor_logo.png"
  launch_url         = "https://ocr.${local.domain}"
  description        = "Docker Registry Storage"
  newtab             = true
  group              = "Infrastructure"
  auth_groups        = [authentik_group.infrastructure.id]
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  client_id          = local.oauth_client_id["harbor"]
  client_secret      = local.oauth_client_secret["harbor"]
  redirect_uris      = ["https://ocr.${local.domain}/c/oidc/callback"]
}

module "oauth2-immich" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name               = "Immich"
  icon_url           = "https://github.com/immich-app/immich/raw/main/docs/static/img/favicon.png"
  launch_url         = "https://photos.${local.domain}"
  description        = "Photo Management"
  newtab             = true
  group              = "Media"
  auth_groups        = [authentik_group.media.id]
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  client_id          = local.oauth_client_id["immich"]
  client_secret      = local.oauth_client_secret["immich"]
  redirect_uris = [
    "https://photos.${local.domain}/auth/login",
    "app.immich:///oauth-callback"
  ]
}

module "oauth2-linkwarden" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name               = "Linkwarden"
  icon_url           = "https://raw.githubusercontent.com/linkwarden/linkwarden/main/assets/logo.png"
  launch_url         = "https://linkwarden.${local.domain}"
  description        = "Self-hosted Web Bookmarks Management"
  newtab             = true
  group              = "Web"
  auth_groups        = [authentik_group.media.id]
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  client_id          = local.oauth_client_id["linkwarden"]
  client_secret      = local.oauth_client_secret["linkwarden"]
  redirect_uris      = ["https://linkwarden.${local.domain}/"]
}

module "oauth2-minio" {
  source                       = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name                         = "MinIO"
  icon_url                     = "https://min.io/resources/img/logo/MINIO_Bird.png"
  launch_url                   = "https://minio.${local.domain}"
  description                  = "S3 Compatible Storage"
  newtab                       = true
  group                        = "Infrastructure"
  auth_groups                  = [authentik_group.infrastructure.id]
  authorization_flow           = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow            = resource.authentik_flow.provider-invalidation.uuid
  client_id                    = local.oauth_client_id["minio"]
  client_secret                = local.oauth_client_secret["minio"]
  include_claims_in_id_token   = false
  additional_property_mappings = formatlist(authentik_property_mapping_provider_scope.openid-minio.id)
  #sub_mode                     = "user_username"
  redirect_uris = ["https://minio.${local.domain}/oauth_callback"]
}

module "oauth2-nextcloud" {
  source                       = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name                         = "Nextcloud"
  icon_url                     = "https://upload.wikimedia.org/wikipedia/commons/6/60/Nextcloud_Logo.svg"
  launch_url                   = "https://cloud.${local.domain}"
  description                  = "Nextcloud"
  newtab                       = true
  group                        = "Groupware"
  auth_groups                  = [authentik_group.nextcloud.id]
  authorization_flow           = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow            = resource.authentik_flow.provider-invalidation.uuid
  client_id                    = local.oauth_client_id["nextcloud"]
  client_secret                = local.oauth_client_secret["nextcloud"]
  include_claims_in_id_token   = false
  additional_property_mappings = formatlist(authentik_property_mapping_provider_scope.openid-nextcloud.id)
  sub_mode                     = "user_username"
  redirect_uris                = ["https://cloud.${local.domain}/apps/oidc_login/oidc"]
}

module "oauth2-openwebui" {
  source                     = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name                       = "Open-WebUI"
  icon_url                   = "https://github.com/open-webui/open-webui/raw/main/static/favicon.png"
  launch_url                 = "https://chat.${local.domain}"
  description                = "AI"
  newtab                     = true
  group                      = "AI"
  auth_groups                = [authentik_group.infrastructure.id]
  authorization_flow         = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow          = resource.authentik_flow.provider-invalidation.uuid
  client_id                  = local.oauth_client_id["openwebui"]
  client_secret              = local.oauth_client_secret["openwebui"]
  include_claims_in_id_token = true
  #additional_property_mappings = formatlist(authentik_property_mapping_provider_scope.openid-outline.id)
  sub_mode      = "user_email"
  redirect_uris = ["https://chat.${local.domain}/auth/oidc.callback"]
}

module "oauth2-outline" {
  source                     = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name                       = "Outline"
  icon_url                   = "https://www.getoutline.com/images/logo.svg"
  launch_url                 = "https://notes.${local.domain}"
  description                = "Notion Self hosted alternative"
  newtab                     = true
  group                      = "Groupware"
  auth_groups                = [authentik_group.infrastructure.id]
  authorization_flow         = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow          = resource.authentik_flow.provider-invalidation.uuid
  client_id                  = local.oauth_client_id["outline"]
  client_secret              = local.oauth_client_secret["outline"]
  include_claims_in_id_token = true
  #additional_property_mappings = formatlist(authentik_property_mapping_provider_scope.openid-outline.id)
  sub_mode      = "user_email"
  redirect_uris = ["https://notes.${local.domain}/auth/oidc.callback"]
}

# module "oauth2-proxmox" {
#   source             = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
#   name               = "Proxmox"
#   icon_url           = "https://www.proxmox.com/images/proxmox/proxmox-logo-color-stacked.png"
#   launch_url         = "https://proxmox.${local.domain}"
#   description        = "Virtualization"
#   newtab             = true
#   group              = "Infrastructure"
#   auth_groups        = [authentik_group.infrastructure.id]
#   authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
#   invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
#   client_id          = local.oauth_client_id["proxmox"]
#   client_secret      = local.oauth_client_secret["proxmox"]
#   redirect_uris = [
#     "https://cobra.${local.domain}",
#     "https://mamba.${local.domain}",
#     "https://cascabel.${local.domain}"
#   ]
# }

module "oauth2-romm" {
  source                     = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name                       = "Romm"
  icon_url                   = "https://github.com/rommapp/romm/blob/d880e64e461b23bb476f2a53f4e229393265055e/frontend/assets/isotipo.png?raw=true"
  launch_url                 = "https://games.${local.domain}"
  description                = "ROM Collection"
  newtab                     = true
  group                      = "Media"
  auth_groups                = [authentik_group.media.id]
  authorization_flow         = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow          = resource.authentik_flow.provider-invalidation.uuid
  client_id                  = local.oauth_client_id["romm"]
  client_secret              = local.oauth_client_secret["romm"]
  include_claims_in_id_token = false
  #sub_mode                     = "user_username"
  redirect_uris = ["https://games.${local.domain}/api/oauth/openid"]
}

module "oauth2-semaphore" {
  source                     = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name                       = "Semaphore"
  icon_url                   = "https://raw.githubusercontent.com/imagegenius/templates/main/unraid/img/semaphore.png"
  launch_url                 = "https://semaphore.${local.domain}"
  description                = "Ansible Automation"
  newtab                     = true
  group                      = "Infrastructure"
  auth_groups                = [authentik_group.infrastructure.id]
  authorization_flow         = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow          = resource.authentik_flow.provider-invalidation.uuid
  client_id                  = local.oauth_client_id["semaphore"]
  client_secret              = local.oauth_client_secret["semaphore"]
  include_claims_in_id_token = true
  #sub_mode                     = "user_username"
  redirect_uris = ["https://semaphore.${local.domain}/api/auth/oidc/authentik/redirect"]
}

module "oauth2-wego" {
  source                     = "git@gitlab.com:mapanare-labs/tm/authentik/oauth2-application.git?ref=main"
  name                       = "GitOps"
  icon_url                   = "https://www.vectorlogo.zone/logos/weaveworks/weaveworks-icon.svg"
  launch_url                 = "https://gitops.${local.domain}"
  description                = "GitOps Management Portal"
  newtab                     = true
  group                      = "Infrastructure"
  auth_groups                = [authentik_group.infrastructure.id]
  authorization_flow         = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow          = resource.authentik_flow.provider-invalidation.uuid
  client_id                  = local.oauth_client_id["gitops"]
  client_secret              = local.oauth_client_secret["gitops"]
  include_claims_in_id_token = true
  #additional_property_mappings = formatlist(authentik_property_mapping_provider_scope.openid-gitops.id)
  sub_mode      = "user_username"
  redirect_uris = ["https://gitops.${local.domain}/oauth2/callback"]
}

module "saml-rancher" {
  source                       = "git@gitlab.com:mapanare-labs/tm/authentik/saml-application.git?ref=main"
  name                         = "Rancher"
  icon_url                     = "https://www.rancher.com/assets/img/logos/rancher-logo-cow-blue.svg"
  launch_url                   = "https://rancher.${local.domain}"
  description                  = "Rancher"
  newtab                       = true
  group                        = "Infrastructure"
  auth_groups                  = [authentik_group.infrastructure.id]
  authorization_flow           = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow            = resource.authentik_flow.provider-invalidation.uuid
  issuer                       = "authentik"
  acs_url                      = "https://rancher.${local.domain}/v1-saml/adfs/saml/acs"
  audience                     = "https://rancher.${local.domain}/v1-saml/adfs/saml/metadata"
  sp_binding                   = "post"
  additional_property_mappings = formatlist(authentik_property_mapping_provider_saml.saml-rancher.id)
}

module "saml-doppler" {
  source   = "git@gitlab.com:mapanare-labs/tm/authentik/saml-application.git?ref=main"
  name     = "Doppler"
  icon_url = "https://dashboard.doppler.com/imgs/logo_color.png"
  #launch_url                   = "https://doppler.${local.domain}"
  description                  = "Doppler"
  newtab                       = true
  group                        = "Infrastructure"
  auth_groups                  = [authentik_group.infrastructure.id]
  authorization_flow           = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow            = resource.authentik_flow.provider-invalidation.uuid
  issuer                       = "https://dashboard.doppler.com/login/sso/saml/metadata/$UUID"
  acs_url                      = "https://dashboard.doppler.com/login/sso/callback/$UUID"
  audience                     = ""
  sp_binding                   = "post"
  additional_property_mappings = [authentik_property_mapping_provider_saml.saml-doppler-username.id, authentik_property_mapping_provider_saml.saml-doppler-email.id]
  name_id_mapping              = authentik_property_mapping_provider_saml.saml-doppler-email.id
}

module "saml-gitlab" {
  source             = "git@gitlab.com:mapanare-labs/tm/authentik/saml-application.git?ref=main"
  name               = "GitLab"
  icon_url           = "https://about.gitlab.com/images/press/press-kit-icon.svg"
  launch_url         = "https://gitlab.${local.domain}"
  description        = "GitLab"
  newtab             = true
  group              = "Infrastructure"
  auth_groups        = [authentik_group.infrastructure.id]
  authorization_flow = resource.authentik_flow.provider-authorization-implicit-consent.uuid
  invalidation_flow  = resource.authentik_flow.provider-invalidation.uuid
  issuer             = "https://gitlab.${local.domain}"
  acs_url            = "https://gitlab.${local.domain}/users/auth/saml/callback"
  audience           = "https://gitlab.${local.domain}"
  sp_binding         = "redirect"
  #additional_property_mappings = [authentik_property_mapping_provider_saml.saml-doppler-username.id,authentik_property_mapping_provider_saml.saml-doppler-email.id]
  #name_id_mapping              = authentik_property_mapping_provider_saml.saml-doppler-email.id
}
