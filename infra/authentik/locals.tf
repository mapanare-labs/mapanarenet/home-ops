locals {
  domain = module.doppler_item.fields["AUTHENTIK_INFRA"]["CLUSTER_DOMAIN"]
  oauth_client_id = {
    "forgejo"    = module.doppler_item.fields["FORGEJO"]["OAUTH_CLIENT_ID"]
    "harbor"     = module.doppler_item.fields["HARBOR"]["OIDC_CLIENT_ID"]
    "immich"     = module.doppler_item.fields["IMMICH"]["IMMICH_OAUTH_CLIENT_ID"]
    "grafana"    = module.doppler_item.fields["GRAFANA"]["OIDC_CLIENT_ID"]
    "linkwarden" = module.doppler_item.fields["LINKWARDEN"]["OIDC_CLIENT_ID"]
    "minio"      = module.doppler_item.fields["MINIO"]["MINIO_IDENTITY_OPENID_CLIENT_ID"]
    "nextcloud"  = module.doppler_item.fields["NEXTCLOUD"]["OIDC_CLIENT_ID"]
    "openwebui"  = module.doppler_item.fields["OPEN_WEBUI"]["OPEN_WEBUI_OIDC_CLIENT_ID"]
    "outline"    = module.doppler_item.fields["OUTLINE"]["OUTLINE_OAUTH_CLIENT_ID"]
    "pgadmin"    = module.doppler_item.fields["PGADMIN"]["OAUTH2_CLIENT_ID"]
    # "proxmox"    = module.doppler_item.fields["PROXMOX"]["OIDC_CLIENT_ID"]
    # "pgadmin"   = module.doppler_item.fields["PROXMOX"]["OAUTH2_CLIENT_ID"]
    "semaphore" = module.doppler_item.fields["SEMAPHORE"]["OAUTH2_CLIENT_ID"]
    "gitops"    = module.doppler_item.fields["WEAVEGITOPS"]["WEAVEGITOPS_OIDC_CLIENT_ID"]
    "romm"      = module.doppler_item.fields["ROMM"]["OIDC_CLIENT_ID"]
    # Añade más clientes según sea necesario
  }
  oauth_client_secret = {
    "forgejo"    = module.doppler_item.fields["FORGEJO"]["OAUTH_CLIENT_SECRET"]
    "harbor"     = module.doppler_item.fields["HARBOR"]["OIDC_CLIENT_SECRET"]
    "immich"     = module.doppler_item.fields["IMMICH"]["IMMICH_OAUTH_CLIENT_SECRET"]
    "grafana"    = module.doppler_item.fields["GRAFANA"]["OIDC_CLIENT_SECRET"]
    "linkwarden" = module.doppler_item.fields["LINKWARDEN"]["OIDC_CLIENT_SECRET"]
    "minio"      = module.doppler_item.fields["MINIO"]["MINIO_IDENTITY_OPENID_CLIENT_SECRET"]
    "nextcloud"  = module.doppler_item.fields["NEXTCLOUD"]["OIDC_CLIENT_PASSWORD"]
    "openwebui"  = module.doppler_item.fields["OPEN_WEBUI"]["OPEN_WEBUI_OIDC_CLIENT_SECRET"]
    "outline"    = module.doppler_item.fields["OUTLINE"]["OUTLINE_OAUTH_CLIENT_SECRET"]
    "pgadmin"    = module.doppler_item.fields["PGADMIN"]["OAUTH2_CLIENT_SECRET"]
    # "proxmox"    = module.doppler_item.fields["PROXMOX"]["OIDC_CLIENT_SECRET"]
    # "pgadmin"   = module.doppler_item.fields["PROXMOX"]["FRONTEND_GA_TRACKING_ID"]
    "semaphore" = module.doppler_item.fields["SEMAPHORE"]["OAUTH2_CLIENT_SECRET"]
    "gitops"    = module.doppler_item.fields["WEAVEGITOPS"]["WEAVEGITOPS_OIDC_CLIENT_SECRET"]
    "romm"      = module.doppler_item.fields["ROMM"]["OIDC_CLIENT_SECRET"]
    # Añade más clientes según sea necesario
  }
}
