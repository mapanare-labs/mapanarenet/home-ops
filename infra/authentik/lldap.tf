# LLDAP Attribute Mapping
data "authentik_property_mapping_source_ldap" "mapping_ldap" {
  managed_list = [
    "goauthentik.io/sources/ldap/default-name",
    "goauthentik.io/sources/ldap/default-mail",
    "goauthentik.io/sources/ldap/ms-givenName",
    "goauthentik.io/sources/ldap/ms-sn",
    "goauthentik.io/sources/ldap/openldap-cn",
    "goauthentik.io/sources/ldap/openldap-uid",
  ]
}

data "authentik_property_mapping_source_ldap" "group_mapping_ldap" {
  managed_list = [
    "goauthentik.io/sources/ldap/openldap-cn",
  ]
}

# Create LDAP Source
resource "authentik_source_ldap" "lldap_source" {
  enabled = true
  name    = "LLDAP"
  slug    = "lldap"

  server_uri    = "ldap://lldap.auth.svc.cluster.local:389"
  start_tls     = false
  bind_cn       = "uid=admin,ou=people,dc=mapanare,dc=net"
  bind_password = module.doppler_item.fields["AUTHENTIK_INFRA"]["LLDAP_PASSWORD"]
  base_dn       = "dc=mapanare,dc=net"

  sync_users          = true
  sync_groups         = true
  sync_users_password = true

  user_path_template  = "LDAP/users"
  additional_user_dn  = "ou=people"
  additional_group_dn = "ou=groups"

  user_object_filter      = "(objectClass=person)"
  group_object_filter     = "(objectClass=groupOfUniqueNames)"
  group_membership_field  = "member"
  object_uniqueness_field = "uid"

  property_mappings = data.authentik_property_mapping_source_ldap.mapping_ldap.ids

  property_mappings_group = data.authentik_property_mapping_source_ldap.group_mapping_ldap.ids
}
