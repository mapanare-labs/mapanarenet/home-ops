# Env init
terraform {
  backend "gcs" {
    bucket      = "20220613-iac-us-east-1"
    prefix      = "mapanarenet/homeops/infra/authentik/prdnpci/terraform.state"
    credentials = "~/.gcp/sdlc-mapanarelabs.json"
  }
}

# Provider information
terraform {
  required_version = ">= 1.5.0"

  required_providers {
    authentik = {
      source  = "goauthentik/authentik"
      version = "2024.12.0"
    }
    doppler = {
      source  = "DopplerHQ/doppler"
      version = "1.13.0"
    }
    sops = {
      source  = "carlpett/sops"
      version = "1.1.1"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.6.3"
    }
  }
}

# Configuration options
provider "authentik" {
  url   = module.doppler_item.fields["AUTHENTIK_INFRA"]["API_URL"]
  token = module.doppler_item.fields["AUTHENTIK_INFRA"]["API_TOKEN_ID"]
  # Optionally set insecure to ignore TLS Certificates
  # insecure = true
}

provider "doppler" {
  doppler_token = data.sops_file.secrets.data["doppler_token"]
}

data "sops_file" "secrets" {
  source_file = "secret.sops.yml"
}

data "doppler_secrets" "this" {}
