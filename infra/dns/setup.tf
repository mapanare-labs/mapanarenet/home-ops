terraform {
  backend "gcs" {
    bucket      = "20220613-iac-us-east-1"
    prefix      = "mapanare-net/hlab/infra/prdnpci/dns/terraform.state"
    credentials = "~/.gcp/sdlc-mapanarelabs.json"
  }
}

# Provider information
terraform {
  required_version = ">= 0.15.0"

  required_providers {
    dns = {
      source  = "hashicorp/dns"
      version = "3.2.3"

    sops = {
      source  = "carlpett/sops"
      version = "0.7.2"
    }
  }
}

# Configuration options
provider "dns" {
  update {
    server        = data.sops_file.secrets.data["internal_dns_server"]
    key_name      = "tsig-key."
    key_algorithm = "hmac-sha256"
    key_secret    = data.sops_file.secrets.data["tsig_key"]
  }
}

data "sops_file" "secrets" {
  source_file = "secret.sops.yml"
}