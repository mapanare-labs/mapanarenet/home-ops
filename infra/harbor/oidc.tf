resource "harbor_config_auth" "oidc" {
  auth_mode          = "oidc_auth"
  primary_auth_mode  = false
  oidc_name          = "authentik"
  oidc_endpoint      = "https://sso.mapanare.net/application/o/harbor/"
  oidc_client_id     = module.doppler_item.fields["HARBOR"]["OIDC_CLIENT_ID"]
  oidc_client_secret = module.doppler_item.fields["HARBOR"]["OIDC_CLIENT_SECRET"]
  oidc_scope         = "openid,profile,email"
  oidc_verify_cert   = true
  oidc_auto_onboard  = true
  oidc_user_claim    = "preferred_username"
  oidc_admin_group   = "authentik Admins"
  oidc_groups_claim  = "groups"
}
