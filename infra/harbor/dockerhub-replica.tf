resource "harbor_project" "dockerhub_replica" {
  name                   = "dockerhub-replica"
  public                 = false # (Optional) Default value is false
  vulnerability_scanning = true  # (Optional) Default vale is true. Automatically scan images on push 
  enable_content_trust   = true  # (Optional) Default vale is false. Deny unsigned images from being pulled 
}

resource "harbor_registry" "dockerhub_registry" {
  provider_name = "docker-hub"
  name          = "Docker Hub"
  endpoint_url  = "https://hub.docker.com"
  access_id     = sensitive(jsondecode(data.doppler_secrets.this.map.DOCKERHUB)["ACCESS_ID"])
  access_secret = sensitive(jsondecode(data.doppler_secrets.this.map.DOCKERHUB)["ACCESS_SECRET"])
}

resource "harbor_replication" "dockerhub_replication" {
  name                   = "dockerhub"
  action                 = "pull"
  registry_id            = harbor_registry.dockerhub_registry.registry_id
  schedule               = "manual"
  dest_namespace         = "dockerhub-replica"
  override               = true
  enabled                = true
  dest_namespace_replace = -1
}