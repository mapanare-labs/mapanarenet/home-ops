resource "harbor_project" "enmanuelmoreira_repository" {
  name                   = "enmanuelmoreira"
  public                 = false # (Optional) Default value is false
  vulnerability_scanning = true  # (Optional) Default vale is true. Automatically scan images on push 
  enable_content_trust   = true  # (Optional) Default vale is false. Deny unsigned images from being pulled 
}


resource "harbor_project" "mapanarelabs_repository" {
  name                   = "mapanarelabs"
  public                 = false # (Optional) Default value is false
  vulnerability_scanning = true  # (Optional) Default vale is true. Automatically scan images on push 
  enable_content_trust   = true  # (Optional) Default vale is false. Deny unsigned images from being pulled 
}


resource "harbor_project" "mapanarenet_repository" {
  name                   = "mapanarenet"
  public                 = false # (Optional) Default value is false
  vulnerability_scanning = true  # (Optional) Default vale is true. Automatically scan images on push 
  enable_content_trust   = true  # (Optional) Default vale is false. Deny unsigned images from being pulled 
}
