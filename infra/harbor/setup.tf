# Env init
terraform {
  backend "gcs" {
    bucket      = "20220613-iac-us-east-1"
    prefix      = "mapanarenet/homeops/infra/harbor/prdnpci/terraform.state"
    credentials = "~/.gcp/sdlc-mapanarelabs.json"
  }
}

# Provider information
terraform {
  required_version = ">= 1.5.0"

  required_providers {
    harbor = {
      source  = "goharbor/harbor"
      version = "3.10.19"
    }
    doppler = {
      source  = "DopplerHQ/doppler"
      version = "1.13.0"
    }
    sops = {
      source  = "carlpett/sops"
      version = "1.1.1"
    }
  }
}

provider "doppler" {
  doppler_token = data.sops_file.secrets.data["doppler_token"]
}

# Configuration options
provider "harbor" {
  url      = module.doppler_item.fields["HARBOR"]["HARBOR_URL"]
  username = module.doppler_item.fields["HARBOR"]["HARBOR_ADMIN_USER"]
  password = module.doppler_item.fields["HARBOR"]["HARBOR_ADMIN_PASSWORD"]
}

data "sops_file" "secrets" {
  source_file = "secret.sops.yml"
}

data "doppler_secrets" "this" {}