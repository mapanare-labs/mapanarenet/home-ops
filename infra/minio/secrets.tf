module "doppler_item" {
  source          = "git::ssh://git@gitlab.com/mapanare-labs/tm/doppler/doppler-item.git?ref=main"
  doppler_project = "home-ops"
  doppler_config  = "prdnpci"
}
