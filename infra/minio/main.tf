locals {
  minio_user_secret = module.doppler_item.fields["MINIO"]["MINIO_USER_SECRET"]
}

resource "minio_s3_bucket" "postgres_backups_bucket" {
  bucket = "cnpg-lxsj5esx"
  acl    = "private"
}

# FORGEJO
module "s3_forgejo" {
  source      = "git@gitlab.com:mapanare-labs/tm/minio-s3.git?ref=main"
  bucket_name = "forgejo"
  # The OP provider converts the fields with toLower!
  user_secret = local.minio_user_secret
}

resource "minio_iam_service_account" "forgejo_sa" {
  target_user = "forgejo"

  depends_on = [module.s3_forgejo]
}

# module "s3_nextcloud" {
#   source      = "git@gitlab.com:mapanare-labs/tm/minio-s3.git?ref=main"
#   bucket_name = "nextcloud"
#   # The OP provider converts the fields with toLower!
#   user_secret = module.doppler_item.fields["MINIO"]["MINIO_USER_SECRET"]
# }

# resource "minio_iam_service_account" "nextcloud_sa" {
#   target_user = "nextcloud"

#   depends_on = [module.s3_nextcloud]
# }

# # OUTLINE
# module "s3_outline" {
#   source      = "git@gitlab.com:mapanare-labs/tm/minio-s3.git?ref=main"
#   bucket_name = "outline"
#   # The OP provider converts the fields with toLower!
#   user_secret = module.doppler_item.fields["MINIO"]["MINIO_USER_SECRET"]
# }

# resource "minio_iam_service_account" "outline_sa" {
#   target_user = "outline"

#   depends_on = [module.s3_outline]
# }

# # Outline custom bucket policy
# data "minio_iam_policy_document" "bucket_policy" {
#   statement {
#     sid       = ""
#     effect    = "Allow"
#     resources = ["arn:aws:s3:::outline"]
#     actions   = ["s3:GetBucketLocation"]

#     principals {
#       type        = "AWS"
#       identifiers = ["*"]
#     }
#   }

#   statement {
#     sid       = ""
#     effect    = "Allow"
#     resources = ["arn:aws:s3:::outline"]
#     actions   = ["s3:ListBucket"]

#     condition {
#       test     = "StringEquals"
#       variable = "s3:prefix"

#       values = [
#         "avatars",
#         "public",
#       ]
#     }

#     principals {
#       type        = "AWS"
#       identifiers = ["*"]
#     }
#   }

#   statement {
#     sid    = ""
#     effect = "Allow"

#     resources = [
#       "arn:aws:s3:::${module.s3_outline.bucket}/avatars*",
#       "arn:aws:s3:::${module.s3_outline.bucket}/public*",
#     ]

#     actions = ["s3:GetObject"]

#     principals {
#       type        = "AWS"
#       identifiers = ["*"]
#     }
#   }
# }
