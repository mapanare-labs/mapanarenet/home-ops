variable "minio_server" {
  type        = string
  default     = "s3.mapanare.net"
  description = "MinIO Host"
}

variable "minio_region" {
  type        = string
  default     = "us-east-1"
  description = "MinIO Region"
}
