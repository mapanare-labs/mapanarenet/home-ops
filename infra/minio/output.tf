output "access_key_forgejo" {
  value = minio_iam_service_account.forgejo_sa.access_key
}

output "secret_key_forgejo" {
  value     = minio_iam_service_account.forgejo_sa.secret_key
  sensitive = true
}

# output "access_key_nextcloud" {
#   value = minio_iam_service_account.nextcloud_sa.access_key
# }

# output "secret_key_nextcloud" {
#   value     = minio_iam_service_account.nextcloud_sa.secret_key
#   sensitive = true
# }

# output "access_key_outline" {
#   value = minio_iam_service_account.outline_sa.access_key
# }

# output "secret_key_outline" {
#   value     = minio_iam_service_account.outline_sa.secret_key
#   sensitive = true
# }
