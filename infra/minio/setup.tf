# Env init
terraform {
  backend "gcs" {
    bucket      = "20220613-iac-us-east-1"
    prefix      = "mapanarenet/homeops/infra/minio/prdnpci/terraform.state"
    credentials = "~/.gcp/sdlc-mapanarelabs.json"
  }
}

# Provider information
terraform {
  required_version = ">= 1.5.0"

  required_providers {
    minio = {
      source  = "aminueza/minio"
      version = ">= 3.2.2"
    }
    doppler = {
      source  = "DopplerHQ/doppler"
      version = "1.13.0"
    }
    sops = {
      source  = "carlpett/sops"
      version = "1.1.1"
    }
  }
}

# Configuration options
provider "minio" {
  minio_server   = var.minio_server
  minio_region   = var.minio_region
  minio_ssl      = true
  minio_user     = module.doppler_item.fields["MINIO"]["MINIO_ROOT_USER"]
  minio_password = module.doppler_item.fields["MINIO"]["MINIO_ROOT_PASSWORD"]
}

provider "doppler" {
  doppler_token = data.sops_file.secrets.data["doppler_token"]
}

data "sops_file" "secrets" {
  source_file = "secret.sops.yml"
}
