# HomeOps

<div align="center">

<img src="https://camo.githubusercontent.com/5b298bf6b0596795602bd771c5bddbb963e83e0f/68747470733a2f2f692e696d6775722e636f6d2f7031527a586a512e706e67" align="center" width="144px" height="144px"/>

### My home operations repository :octocat:

_... managed with Flux, GitLab CI_ 🤖

</div>

---

## 📖 Overview

This is a mono repository for my home infrastructure and Kubernetes cluster. I try to adhere to Infrastructure as Code (IaC) and GitOps practices using the tools like [Ansible](https://www.ansible.com/), [Terraform](https://www.terraform.io/), [Kubernetes](https://kubernetes.io/), [Flux](https://github.com/fluxcd/flux2), and [GitLab CI](https://docs.gitlab.com/ee/ci/).

---

## Create Flux Namespace

```bash
kubectl create ns flux-system
```

## Age

### Create age key and create secret

```bash
age-keygen -o ~/.age/key.txt
```

```bash
kubectl -n flux-system create secret generic sops-age \
  --from-file=age.agekey=/home/user/.age/key.txt
```

## GitLab

### Create ssh keypair

```bash
ssh-keygen -f ~/.ssh/fluxcd -P ""
```

### Export GiLab known_host

```bash
export GL_SSH=$(ssh-keyscan gitlab.com) 
```

### Create secret

```bash
kubectl create secret generic gitlab-deploy-key --from-file=identity=/home/user/.ssh/fluxcd --from-file=identity.pub=/home/user/.ssh/fluxcd.pub --from-literal=known_hosts=$GL_SSH --namespace flux-system
```

## Flux

### Bootstrap flux

```bash
kubectl apply --server-side --kustomize ./kubernetes/bootstrap/flux
```

### Apply Cluster Configuration

_These cannot be applied with `kubectl` in the regular fashion due to be encrypted with sops_

```bash
sops --decrypt kubernetes/bootstrap/flux/age-key.sops.yaml | kubectl apply -f -
```

```bash
sops --decrypt kubernetes/bootstrap/flux/gitlab-deploy-key.sops.yaml | kubectl apply -f -
```

```bash
sops --decrypt kubernetes/flux/vars/cluster-secrets.sops.yaml | kubectl apply -f -
```

```bash
kubectl apply -f kubernetes/flux/vars/cluster-settings.yaml
```

### Kick off Flux applying this repository

```bash
kubectl apply --server-side --kustomize ./kubernetes/flux/config
```

---

## 🤝 Gratitude and Thanks

- [billimek/k8s-gitops](https://github.com/billimek/k8s-gitops)
- [carpenike/k8s-gitops](https://github.com/carpenike/k8s-gitops)
- [onedr0p/home-ops](https://github.com/onedr0p/home-ops)
- [buroa/k8s-gitops](https://github.com/buroa/k8s-gitops)
- [lenaxia/home-ops-dev](https://github.com/lenaxia/home-ops-dev)
- [bjw-s/home-ops](https://github.com/bjw-s/home-ops)

---