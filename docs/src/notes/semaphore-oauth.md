## Ansible Semaphore oAuth2

Hello together,

Well for what it's worth, I have a working setup now.

Let me right down the working config, so it might help someone else:

The oidc_providers of config.json for Semaphore:

```json
"oidc_providers": {
            "authentik": {
                "display_name": "Sign in with SSO",
                "provider_url": "https://auth.authentik.com/application/o/semaphore/",
                "client_id": "<<Client_ID>>",
                "client_secret": "<<Client_Secret>>",
                "redirect_url": "https://semaphore.example.com/api/auth/oidc/authentik/redirect/",
                "scopes": ["email", "openid", "profile"],
                "username_claim": "preferred_username",
                "name_claim": "preferred_username"
            }
        },
```

Authentik side:
Use the OAuth2/OpenID Provider option as a provider

Redirect URIs/Origins (RegEx): https://semaphore.example.com/api/auth/oidc/authentik/redirect/
Scopes: email, openid, profile
Subject mode: Based on the User's hashed ID
Check the box Include claims in id_token

Use the default settings for the Application

Using this settings semaphore is able to create a user based on the scopes provided by Authentik.
For the user name created inside Semaphore be aware that it will look something like this:
Authentik Email : user.name@mail.com
Semaphore User: user.name_fljfXdkdEdd

Let me know if someone else could get the OIDC setup working for them.

## References

- <https://github.com/ansible-semaphore/semaphore/discussions/1663>