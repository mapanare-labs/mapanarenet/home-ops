# Nvidia GPU Operator Setup on Ubuntu 22.04 Cluster

This guide will walk you through the process of setting up the Nvidia GPU Operator on a Ubuntu 22.04 (Focal) cluster. Specifically, we're going to install Nvidia drivers, reboot the node, test it, and modify the GPU Operator manifest file.

Remember, if you're working with a Tesla GPU, ensure you install the Tesla drivers.

## Add NVIDIA Container Runtime Repo

Start by adding the Ubuntu package repository to your system.

```shell
curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
  && curl -s -L https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list | \
    sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
    sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list
```

## Install GPU Drivers
Next, you'll want to update your package lists for the new repositories.

```
sudo apt update
sudo apt install -y nvidia-container-runtime cuda-drivers-fabricmanager-550 nvidia-headless-550-server nvidia-utils-550-server
```

## Reboot Node
Reboot your node to ensure the changes take effect.

```
sudo systemctl reboot
```

# Test GPU Functionality
After your node restarts, you can confirm the Nvidia drivers are working properly with the following command:
```
nvidia-smi

or 


grep nvidia /var/lib/rancher/k3s/agent/etc/containerd/config.toml

[plugins."io.containerd.grpc.v1.cri".containerd.runtimes."nvidia"]
[plugins."io.containerd.grpc.v1.cri".containerd.runtimes."nvidia".options]
BinaryName = "/usr/bin/nvidia-container-runtime"
```

# References

<https://docs.k3s.io/advanced#alternative-container-runtime-support>